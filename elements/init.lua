local ui = require("ui.main")
local uie = require("ui.elements.main")

require("ui.elements.basic")
require("ui.elements.layout")
require("ui.elements.input")
require("ui.elements.window")
require("ui.elements.scroll")

return uie
